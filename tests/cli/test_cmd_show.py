import json
import tomllib
from pathlib import Path
from unittest.mock import patch

import yaml
from click.testing import CliRunner

from autosd_demo import AUTOSD_DEMO_DATA_PATH, settings
from autosd_demo.cli import cli


def test_cmd_show_config(tmp_dir):
    settings_path = tmp_dir / ".autosd-demo.toml"
    settings_path.touch()

    settings.reload_settings()
    runner = CliRunner()

    result = runner.invoke(cli, ["show", "config"])
    assert result.exit_code == 0
    assert tomllib.loads(result.output)["base_dir"] == tmp_dir.as_posix()

    result = runner.invoke(cli, ["show", "config", "-o", "json"])
    assert result.exit_code == 0
    assert json.loads(result.output)["base_dir"] == tmp_dir.as_posix()

    result = runner.invoke(cli, ["show", "config", "-o", "yaml"])
    assert result.exit_code == 0
    assert yaml.safe_load(result.output)["base_dir"] == tmp_dir.as_posix()


@patch("autosd_demo.cli.cmd_show.get_build_env_config")
def test_cmd_show_osbuild_manifest(mock_get_build_env_config):
    mock_get_build_env_config.return_value = {
        "conf": {
            "name": "demotest",
            "template_image_path": Path(
                AUTOSD_DEMO_DATA_PATH, "image.mpp.yaml.j2"
            ).as_posix(),
            "containers": {},
        },
        "build": {},
    }

    runner = CliRunner()
    result = runner.invoke(cli, ["show", "osbuild-manifest"])
    assert result.exit_code == 0
