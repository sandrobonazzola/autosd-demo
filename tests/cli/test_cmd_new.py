from click.testing import CliRunner

from autosd_demo.cli import cli


def test_cmd_new(tmp_dir):
    runner = CliRunner()
    result = runner.invoke(cli, ["new", "new_project"])
    assert result.exit_code == 0
    assert tmp_dir.joinpath("new_project").is_dir()
    assert tmp_dir.joinpath("new_project", ".autosd-demo.toml").is_file()


def test_cmd_new_invalid_project(tmp_dir):
    new_project = tmp_dir / "new_project"
    new_project.mkdir()

    runner = CliRunner()
    result = runner.invoke(cli, ["new", "new_project"])
    assert result.exit_code == 1
    assert "already exists" in result.output
