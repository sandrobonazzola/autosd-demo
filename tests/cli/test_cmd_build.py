from unittest.mock import MagicMock, patch

from click.testing import CliRunner

from autosd_demo.cli import cli


@patch("autosd_demo.cli.cmd_build.get_build_env_config")
@patch("autosd_demo.cli.cmd_build.get_executor")
def test_build_with_local_env(mock_executor, mock_get_build_env_config):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock()
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx

    runner = CliRunner()
    runner.invoke(cli, ["build"])

    mock_get_build_env_config.assert_called_once_with(None)
    mock_executor.assert_called_once_with(remote=False, become_required=True)
    mock_executor_ctx.assert_called_once_with(host=None)
    mock_run_playbook.assert_any_call(
        "sudo-nopassword-enable.yaml", extra_vars=mock_get_build_env_config.return_value
    )
    mock_run_playbook.assert_any_call(
        "build-prepare-env.yaml", extra_vars=mock_get_build_env_config.return_value
    )
    mock_run_playbook.assert_any_call(
        "build-download-image.yaml", extra_vars=mock_get_build_env_config.return_value
    )
    mock_run_playbook.assert_any_call(
        "build-clean-env.yaml", extra_vars=mock_get_build_env_config.return_value
    )
    mock_run_playbook.assert_any_call(
        "sudo-nopassword-disable.yaml",
        extra_vars=mock_get_build_env_config.return_value,
    )
    assert mock_run_playbook.call_count == 5


@patch("autosd_demo.cli.cmd_build.get_build_env_config")
@patch("autosd_demo.cli.cmd_build.get_executor")
def test_setup_with_remote_env(mock_executor, mock_get_build_env_config):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock()
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx

    runner = CliRunner()
    runner.invoke(cli, ["build", "-H", "foobar"])

    mock_executor.assert_called_once_with(remote=True, become_required=True)
    mock_executor_ctx.assert_called_once_with(host="foobar")
    mock_run_playbook.assert_any_call(
        "sudo-nopassword-enable.yaml", extra_vars=mock_get_build_env_config.return_value
    )
    mock_run_playbook.assert_any_call(
        "build-prepare-env.yaml", extra_vars=mock_get_build_env_config.return_value
    )
    mock_run_playbook.assert_any_call(
        "build-download-image.yaml", extra_vars=mock_get_build_env_config.return_value
    )
    mock_run_playbook.assert_any_call(
        "build-clean-env.yaml", extra_vars=mock_get_build_env_config.return_value
    )
    mock_run_playbook.assert_any_call(
        "sudo-nopassword-disable.yaml",
        extra_vars=mock_get_build_env_config.return_value,
    )
    assert mock_run_playbook.call_count == 5


@patch("autosd_demo.cli.cmd_build.get_build_env_config")
@patch("autosd_demo.cli.cmd_build.get_executor")
def test_build_with_local_env_and_no_clean(mock_executor, mock_get_build_env_config):
    mock_executor_ctx = MagicMock()
    mock_run_playbook = MagicMock()
    mock_executor_ctx.return_value.__enter__.return_value.run_playbook = (
        mock_run_playbook
    )
    mock_executor.return_value = mock_executor_ctx

    runner = CliRunner()
    runner.invoke(cli, ["build", "--no-clean"])

    mock_get_build_env_config.assert_called_once_with(None)
    mock_executor.assert_called_once_with(remote=False, become_required=True)
    mock_executor_ctx.assert_called_once_with(host=None)
    mock_run_playbook.assert_any_call(
        "sudo-nopassword-enable.yaml", extra_vars=mock_get_build_env_config.return_value
    )
    mock_run_playbook.assert_any_call(
        "build-prepare-env.yaml", extra_vars=mock_get_build_env_config.return_value
    )
    mock_run_playbook.assert_any_call(
        "build-download-image.yaml", extra_vars=mock_get_build_env_config.return_value
    )
    mock_run_playbook.assert_any_call(
        "sudo-nopassword-disable.yaml",
        extra_vars=mock_get_build_env_config.return_value,
    )
    assert mock_run_playbook.call_count == 4
