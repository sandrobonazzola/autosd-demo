import os
from pathlib import Path

import pytest


@pytest.mark.integration
def test_cli_new(empty_ws: Path):
    assert os.system("autosd-demo new test") == 0
    assert empty_ws.joinpath("test", ".autosd-demo.toml").exists()
