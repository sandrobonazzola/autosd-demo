import os
from pathlib import Path

import pytest


@pytest.mark.integration
def test_cli_build(simple_ws: Path):
    cfg = simple_ws.joinpath(".autosd-demo.toml")
    assert cfg.exists()
    assert os.system("autosd-demo setup") == 0

    build_cmd = (
        "autosd-demo build -e rootfs --build-id simple-ws-integration --no-clean"
    )

    assert os.system(build_cmd) == 0
