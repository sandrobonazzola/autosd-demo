from unittest import mock

from autosd_demo.ansible.callbacks import click_output


def test_click_output_metadata():
    cb_mod = click_output.CallbackModule()
    assert cb_mod.CALLBACK_VERSION == 2.0
    assert cb_mod.CALLBACK_TYPE == "stdout"
    assert cb_mod.CALLBACK_NAME == "autosd.demo.click_output"
    assert cb_mod.CALLBACK_NEEDS_ENABLED is False


@mock.patch("rich.print")
def test_click_output_on_failed(mock_rich_print):
    cb_mod = click_output.CallbackModule()
    fake_result = mock.Mock()
    fake_result._task.get_name.return_value = "test_task"
    cb_mod.v2_runner_on_failed(fake_result)
    mock_rich_print.assert_called_once_with("[red][■] test_task[/red]")


@mock.patch("rich.print")
def test_click_output_ok_changed(mock_rich_print):
    cb_mod = click_output.CallbackModule()
    fake_result = mock.Mock()
    fake_result._task.get_name.return_value = "test_task"
    fake_result._result.get.return_value = True
    cb_mod.v2_runner_on_ok(fake_result)
    mock_rich_print.assert_called_once_with(
        "[green][[/green][yellow]🗸[/yellow][green]][/green]", "[green]test_task[/green]"
    )


@mock.patch("rich.print")
def test_click_output_ok_unchanged(mock_rich_print):
    cb_mod = click_output.CallbackModule()
    fake_result = mock.Mock()
    fake_result._task.get_name.return_value = "test_task"
    fake_result._result.get.return_value = False
    cb_mod.v2_runner_on_ok(fake_result)
    mock_rich_print.assert_called_once_with("[green][🗸] test_task[/green]")
