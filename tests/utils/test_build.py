from unittest.mock import patch

import pytest

from autosd_demo.utils.build import get_build_env_config


@patch("autosd_demo.utils.build.settings")
def test_get_build_env_config_with_local_env(mock_settings):
    mock_settings.as_dict.return_value = {
        "base_dir": "~/src",
        "build_dir": "~/src/build",
    }
    build_env = get_build_env_config(None)
    assert build_env["conf"]["base_dir"] == "~/src"
    assert build_env["conf"]["build_dir"] == "~/src/build"
    assert build_env["build"]["base_dir"] == "~/src"
    assert build_env["build"]["build_dir"] == "~/src/build"
    assert build_env["build"]["is_remote"] is False


@pytest.mark.parametrize(
    "settings_data, expected",
    [
        (
            {
                "remote_host": {
                    "foo": {"base_dir": "/home/foo", "build_dir": "/home/foo/build"}
                }
            },
            {"base_dir": "/home/foo", "build_dir": "/home/foo/build"},
        ),
        (
            {
                "remote_host": {
                    "foo": {
                        "base_dir": "/home/foo",
                    }
                }
            },
            {"base_dir": "/home/foo", "build_dir": "/home/foo/build"},
        ),
        ({"remote_host": {"foo": {}}}, {"base_dir": "~", "build_dir": "~/build"}),
        ({}, {"base_dir": "~", "build_dir": "~/build"}),
    ],
)
@patch("autosd_demo.utils.build.settings")
def test_get_build_env_config_with_remote_env(mock_settings, settings_data, expected):
    mock_settings.as_dict.return_value = settings_data
    result = get_build_env_config("foo")
    assert "conf" in result
    assert "build" in result
    assert result["build"]["base_dir"] == expected["base_dir"]
    assert result["build"]["build_dir"] == expected["build_dir"]
