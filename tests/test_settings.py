from pathlib import Path

import pytest

from autosd_demo.settings import Settings, enrich_container_settings


def test_settings(tmp_dir):
    settings_path = tmp_dir / ".autosd-demo.toml"
    settings_path.touch()

    settings = Settings()
    assert Path(settings.base_dir) == tmp_dir


enrich_container_settings_test_cases = [
    (
        {"containers": {"test_container": {}}},
        "test_container",
        "unit",
        "description",
        "test_container container",
    ),
    (
        {"containers": {"test_container": {}}},
        "test_container",
        "container",
        "container_name",
        "test_container",
    ),
    (
        {"containers": {"test_container": {"version": "0.1"}}},
        "test_container",
        "container",
        "image",
        "localhost/test_container:0.1",
    ),
    (
        {"containers": {"test_container": {}}},
        "test_container",
        "install",
        "wanted_by",
        "multi-user.target",
    ),
    (
        {"containers": {"test_container": {}}},
        "test_container",
        "service",
        "restart",
        "always",
    ),
    (
        {"containers": {"test_container": {"systemd": {}}}},
        "test_container",
        "service",
        "restart",
        "always",
    ),
    (
        {"containers": {"test_container": {"systemd": {"service": {}}}}},
        "test_container",
        "service",
        "restart",
        "always",
    ),
    (
        {
            "containers": {
                "test_container": {"systemd": {"service": {"restart": "never"}}}
            }
        },
        "test_container",
        "service",
        "restart",
        "never",
    ),
]


@pytest.mark.parametrize(
    "settings,container_name,section,key,expected", enrich_container_settings_test_cases
)
def test_enrich_container_settings(settings, container_name, section, key, expected):
    enrich_container_settings(settings)
    assert settings["containers"][container_name]["systemd"][section][key] == expected
