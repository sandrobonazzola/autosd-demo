import getpass
from unittest.mock import MagicMock, patch

import pytest
from fabric import Config as FabricConfig
from fabric import Connection
from invoke import Config, Context
from invoke.exceptions import AuthFailure

import autosd_demo
import autosd_demo.utils.ansible
from autosd_demo.core.executor import ensure_required_access, get_executor


@patch("autosd_demo.core.executor.PlaybookCLI")
def test_executor_ansible_executor(playbook_cli):
    cli = MagicMock()
    cli.parse = MagicMock()
    cli.run = MagicMock()
    playbook_cli.return_value = cli

    local_executor = get_executor()("localhost")
    local_executor.run_playbook("test.yaml")

    playbook_cli.assert_called_once_with(
        [
            "ansible-playbook",
            "-i",
            "localhost,",
            "-e",
            "ansible_connection=local",
            f"{autosd_demo.AUTOSD_DEMO_DATA_PATH}/ansible/test.yaml",
        ]
    )
    cli.parse.assert_called_once()
    cli.run.assert_called_once()


@patch("autosd_demo.core.executor.PlaybookCLI")
def test_executor_ansible_executor_with_extra_vars(playbook_cli):
    cli = MagicMock()
    cli.parse = MagicMock()
    cli.run = MagicMock()
    playbook_cli.return_value = cli

    local_executor = get_executor()("localhost")
    local_executor.run_playbook("test.yaml", extra_vars={"conf": {}})

    assert playbook_cli.call_args.args[0][6].endswith("extra-vars.yaml")
    playbook_cli.assert_called_once_with(
        [
            "ansible-playbook",
            "-i",
            "localhost,",
            "-e",
            "ansible_connection=local",
            "-e",
            playbook_cli.call_args.args[0][6],
            f"{autosd_demo.AUTOSD_DEMO_DATA_PATH}/ansible/test.yaml",
        ]
    )
    cli.parse.assert_called_once()
    cli.run.assert_called_once()


@patch("autosd_demo.core.executor.settings")
@patch("autosd_demo.core.executor.PlaybookCLI")
def test_executor_get_executor_remote(playbook_cli, mock_settings):
    cli = MagicMock()
    cli.parse = MagicMock()
    cli.run = MagicMock()
    playbook_cli.return_value = cli

    mock_settings.remote_host = {
        "myhost1": {"address": "myhost.mydomain.org", "user": "admin", "port": 2222}
    }
    mock_settings.__contains__.return_value = True

    remote_executor = get_executor(remote=True)("myhost1")
    remote_executor.run_playbook("test.yaml")

    playbook_cli.assert_called_once_with(
        [
            "ansible-playbook",
            "-i",
            "myhost.mydomain.org,",
            "-u",
            "admin",
            "-e",
            "ansible_ssh_port=2222",
            f"{autosd_demo.AUTOSD_DEMO_DATA_PATH}/ansible/test.yaml",
        ]
    )

    cli.parse.assert_called_once()
    cli.run.assert_called_once()


@patch("autosd_demo.core.executor.PlaybookCLI")
def test_executor_get_executor_remote_with_extra_vars(playbook_cli):
    cli = MagicMock()
    cli.parse = MagicMock()
    cli.run = MagicMock()
    playbook_cli.return_value = cli

    remote_executor = get_executor(remote=True)("myhost1")
    remote_executor.run_playbook("test.yaml", extra_vars={"conf": {}})

    assert playbook_cli.call_args.args[0][8].endswith("extra-vars.yaml")
    playbook_cli.assert_called_once_with(
        [
            "ansible-playbook",
            "-i",
            "myhost1,",
            "-u",
            getpass.getuser(),
            "-e",
            "ansible_ssh_port=22",
            "-e",
            playbook_cli.call_args.args[0][8],
            f"{autosd_demo.AUTOSD_DEMO_DATA_PATH}/ansible/test.yaml",
        ]
    )

    cli.parse.assert_called_once()
    cli.run.assert_called_once()


@patch("autosd_demo.core.executor.Context")
def test_ensure_required_access_with_local_and_sudo_granted(mock_context):
    mock_ctx = MagicMock()
    mock_ctx.run.return_value.ok = True
    mock_ctx.config = Config()
    mock_context.return_value = mock_ctx

    credentials = ensure_required_access(become_required=True, remote=None)
    mock_ctx.run.assert_called_once_with("sudo -n true", hide="both", warn=True)

    assert credentials["sudo"] is None
    assert credentials["ssh"] is None


@patch("autosd_demo.core.executor.click")
@patch("autosd_demo.core.executor.Context")
def test_ensure_required_access_with_local_and_sudo_pass_required(
    mock_context, mock_click
):
    mock_ctx = MagicMock()
    mock_ctx.run.return_value.ok = False
    mock_ctx.config = Config()
    mock_context.return_value = mock_ctx
    mock_click.prompt.return_value = "SudoPass"

    credentials = ensure_required_access(become_required=True, remote=None)
    mock_ctx.run.assert_called_once_with("sudo -n true", hide="both", warn=True)
    mock_ctx.sudo.assert_called_once_with("id", hide="both", warn=True)

    assert credentials["sudo"] == "SudoPass"
    assert credentials["ssh"] is None


@patch("autosd_demo.core.executor.click")
@patch("autosd_demo.core.executor.Context")
def test_ensure_required_access_with_local_and_sudo_auth_failure(
    mock_context, mock_click
):
    mock_ctx = MagicMock()
    mock_ctx.run.return_value.ok = False
    mock_ctx.config = Config()
    mock_context.return_value = mock_ctx
    mock_click.prompt.return_value = "WrongPassword"
    mock_ctx.sudo.side_effect = AuthFailure(None, "")

    with pytest.raises(SystemExit):
        ensure_required_access(become_required=True, remote=None)

    mock_ctx.run.assert_called_once_with("sudo -n true", hide="both", warn=True)
    mock_ctx.sudo.assert_called_once_with("id", hide="both", warn=True)


@patch("autosd_demo.core.executor.click")
@patch("autosd_demo.core.executor.Connection")
def test_ensure_required_access_with_remote(mock_context, mock_click):
    remote_info = {"address": "address", "port": "22", "user": "user"}
    mock_ctx = MagicMock()
    mock_ctx.run.return_value.ok = False
    mock_ctx.config = FabricConfig()
    mock_context.return_value = mock_ctx
    mock_click.prompt.return_value = "SudoPass"

    credentials = ensure_required_access(become_required=True, remote=remote_info)
    mock_ctx.run.assert_called_once_with("sudo -n true", hide="both", warn=True)
    mock_ctx.sudo.assert_called_once_with("id", hide="both", warn=True)

    assert credentials["sudo"] == "SudoPass"
    assert credentials["ssh"] is None


def test_executor_context_manager():
    with get_executor()("localhost") as executor:
        assert isinstance(executor, Context)

    with get_executor(remote=True)("myhost") as executor:
        assert isinstance(executor, Connection)
