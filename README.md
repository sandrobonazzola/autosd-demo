# AutoSD Demo

## Requirements

To use AutoSD Demo, you'll need the following requirements installed on your system:

- Python
- Podman

## Installation

You can easily install `autosd-demo` using `pip`:

```bash
pip install autosd-demo
```

### Developer Installation

If you're a developer and want to contribute to AutoSD Demo or use the latest development version, follow these steps:

1. Clone the Git repository:

   ```bash
   git clone https://gitlab.com/roberto-majadas/autosd-demo
   ```

2. Create a virtual environment (optional but recommended):

   ```bash
   python -m venv venv
   ```

3. Activate the virtual environment:

   - On macOS and Linux:

     ```bash
     source venv/bin/activate
     ```

   - On Windows:

     ```bash
     .\venv\Scripts\activate
     ```

4. Install `autosd-demo` in development mode:

   ```bash
   pip install -r requirements/dev.txt
   pip install -e .
   ```

### Testing Requirements

If you want to enable testing requirements, use the following command:

```bash
pip install -r requirements/dev.txt
```

To run the tests, execute:

```bash
tox -e py
```

### Linters

To run the linters, execute:

```bash
tox -e lint
```

## Usage

Once `autosd-demo` is installed, you can run it in your terminal by using the following command:

```bash
autosd-demo -h
```

## Contribution

If you're interested in contributing to AutoSD Demo, please read the [CONTRIBUTING.md](CONTRIBUTING.md) file in this repository.

## License

AutoSD Demo is released under the MIT License. See the [LICENSE](LICENSE) file for more details.


## Authors

- Roberto Majadas
