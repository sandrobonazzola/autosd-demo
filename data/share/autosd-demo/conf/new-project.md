# AutoSD Demo Configuration file

## Environments

Autosd-demo provides support for a multi-environment configuration. This feature will allow you to define different
configurations or components per environment. The default environment is [default]. If you want to create new
environments you only have to create different fist sections of the configuration like: [local], [devel], [prod]...

Every configuration defined in [default] would be inherited the new environments. For example:

```toml
[default]
name="mydemo"
version="0.0.1"

[devel]
version="0.0.1-dev"

[local]
debug=true
```

## Containers

If you want to include containers in your autosd image you would need to include a section like this:

```toml
[default.containers.my_app]
registry="quay.io"
namespace="myproject"
image="myapp"
version="latest"
```

And if you want to install a custom container developed locally, you could add your Containerfile and all the required
files to a folder like `containers/my_app` in your project. The container will be built and installed in the image.

```toml
[default.containers.my_app]
image="myapp"
version="latest"
container_context="containers/my_app"
container_file="Containerfile"
```

By the default a systemd container file would be generated and included in the osbuild image but if you need to modify
any aspect of this systemd file you could do it as follows:

```toml
[default.containers.my_app.systemd.unit]
description="this is my container"
requires="otherapp.socket"

[default.containers.my_app.systemd.container]
container_name="my_new_app"
exec="/usr/bin/myapp -d"
volume=[
    "/var:/var",
    "/home/user:/home/user"
]
```

## RPM packages and repositories

Sometimes you could need to install packages or repositories in the image, in the root filesystem or in the qm filesystem.
If that is your case you can install packages and/or add rpm repositories as follows:

```toml
[default.rpm.extra_packages]    # "extra_packages" is an ID
packages=["vim", "openssh-server"]

[default.rpm.qm_extra_packages]
packages=["vim"]
layer="qm"
```

and in order to add repositories...

```toml
[default.rpm_repo.my_copr_repo]    # "my_copr_repo" is an ID
baseurl="https://download.copr.fedorainfracloud.org/results/my_corpr/my_packages/"

[default.rpm_repo.my_qm_copr_repo]
baseurl="https://download.copr.fedorainfracloud.org/results/my_qm_copr/my_packages/"
layer="qm"
```

In both cases, rpm or rpm_repos, you could add multiple sections at your convenience using different ID's.
