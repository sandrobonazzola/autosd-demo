stages:
 - checks
 - tests
 - integration
 - deploy

default:
  image: python:latest
  cache:                      # Pip's cache doesn't store the python packages
    paths:                    # https://pip.pypa.io/en/stable/topics/caching/
      - .cache/pip
  before_script:
    - python -V               # Print out python version for debugging
    - python -m venv venv
    - source venv/bin/activate
    - pip install tox


variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"


linters:
  stage: checks
  script:
    - pip install -r requirements/linters.txt
    - tox -e linters

typing:
  stage: checks
  script:
    - pip install -r requirements/typing.txt
    - tox -e typing

tests:
  stage: tests
  script:
    - pip install -r requirements/tests.txt
    - tox -e py -- --junitxml=report.xml
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    reports:
      junit: report.xml
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml

integration test:
  image: fedora:latest
  stage: integration
  before_script:
    - dnf install -y python python-pip
    - python -V
    - python -m venv venv
    - source venv/bin/activate
    - pip install tox
  script:
    - tox -e integration

publish internal python pkg:
  stage: deploy
  script:
    - pip install -r requirements/build.txt
    - COMMITS_NUM_SINCE_LAST_TAG=$(git log $(git describe --tags --abbrev=0)..HEAD --oneline | wc -l)
    - COMMIT_HASH=$(git log -1 --pretty=format:%h)
    - export AUTOSD_DEMO_GIT_RELEASE_VERSION=".dev$COMMITS_NUM_SINCE_LAST_TAG+$COMMIT_HASH"
    - flit build
    - twine check dist/*
    - export PYPI_SERVER="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi"
    - export PYPI_USER="gitlab-ci-token"
    - export PYPI_PASSWORD="${CI_JOB_TOKEN}"
    - twine upload --repository-url ${PYPI_SERVER} --username ${PYPI_USER} --password ${PYPI_PASSWORD} dist/*
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH

publish internal container image:
  stage: deploy
  image: quay.io/buildah/stable:latest
  variables:
    STORAGE_DRIVER: vfs
    BUILDAH_FORMAT: docker
    BUILDAH_ISOLATION: chroot
  before_script:
    - buildah version
    - dnf install -y git
  script:
    # Set gitlab pypi index url
    - export PIPY_INDEX_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/pypi/simple"
    # Get autosd-demo version
    - export AUTOSD_DEMO_PKG_VERSION=$(cat src/autosd_demo/__init__.py | grep __version__ | sed -e "s/__version__\s*=\s*\"//g" | sed -e "s/\".*$//g")
    # Get autosd-demo git release version suffix
    - COMMITS_NUM_SINCE_LAST_TAG=$(git log $(git describe --tags --abbrev=0)..HEAD --oneline | wc -l)
    - COMMIT_HASH=$(git log -1 --pretty=format:%h)
    - export AUTOSD_DEMO_GIT_RELEASE_VERSION=".dev$COMMITS_NUM_SINCE_LAST_TAG+$COMMIT_HASH"
    # Build image
    - echo $CI_REGISTRY_PASSWORD | buildah login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
    - buildah bud --build-arg PIPY_INDEX_URL=${PIPY_INDEX_URL} --build-arg AUTOSD_DEMO_PKG_VERSION=${AUTOSD_DEMO_PKG_VERSION} --build-arg AUTOSD_DEMO_GIT_RELEASE_VERSION=${AUTOSD_DEMO_GIT_RELEASE_VERSION} --tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA containers/Containerfile
    - buildah tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA $CI_REGISTRY_IMAGE:${AUTOSD_DEMO_PKG_VERSION}.dev$COMMITS_NUM_SINCE_LAST_TAG
    - buildah tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA $CI_REGISTRY_IMAGE:latest
    - buildah push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA
    - buildah push $CI_REGISTRY_IMAGE:${AUTOSD_DEMO_PKG_VERSION}.dev$COMMITS_NUM_SINCE_LAST_TAG
    - buildah push $CI_REGISTRY_IMAGE:latest
  needs:
    - publish internal python pkg
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH

pages:
  stage: deploy
  script:
    - pip install -e .
    - pip install -r requirements/docs.txt
    - sphinx-build -b html docs public
  artifacts:
    paths:
    - public
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
