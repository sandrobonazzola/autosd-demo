from pathlib import Path

from autosd_demo.settings import settings


def get_build_env_config(host):
    conf = {key.lower(): val for key, val in settings.as_dict().items()}
    is_remote = True if host else False
    build_env = {"conf": conf, "build": {}}
    build_env["build"]["is_remote"] = is_remote
    if is_remote:
        if "remote_host" in conf and host in conf["remote_host"]:
            if "base_dir" not in conf["remote_host"][host]:
                build_env["build"]["base_dir"] = "~"
            else:
                build_env["build"]["base_dir"] = conf["remote_host"][host]["base_dir"]

            if "build_dir" not in conf["remote_host"][host]:
                build_env["build"]["build_dir"] = str(
                    Path(build_env["build"]["base_dir"]) / "build"
                )
            else:
                build_env["build"]["build_dir"] = conf["remote_host"][host]["build_dir"]
        else:
            build_env["build"]["base_dir"] = "~"
            build_env["build"]["build_dir"] = "~/build"
    else:
        build_env["build"]["base_dir"] = conf["base_dir"]
        build_env["build"]["build_dir"] = conf["build_dir"]
    return build_env
