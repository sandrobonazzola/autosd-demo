import sys
from contextlib import chdir
from pathlib import Path

import click
from jinja2 import Environment, FileSystemLoader

from autosd_demo import AUTOSD_DEMO_DATA_PATH


@click.command()
@click.option("-n", "--name", help="name of the project")
@click.option(
    "-v",
    "--version",
    default="0.0.1",
    help="version of the project",
)
@click.argument("project_dir", required=True)
def new(name, version, project_dir):
    """Create a new autosd-demo"""
    project_path = Path(project_dir)
    if project_path.exists():
        click.echo("The folder already exists")
        sys.exit(1)

    project_path.mkdir(parents=True, exist_ok=True)

    with chdir(project_path):
        conf_path = AUTOSD_DEMO_DATA_PATH / "conf"
        env = Environment(loader=FileSystemLoader(conf_path))
        template = env.get_template("new-project.toml.j2")

        with open(conf_path / "new-project.md") as fd:
            documentation = fd.read().splitlines()

        output = template.render(
            name=name if name else project_path.name,
            version=version,
            documentation=documentation,
        )
        with open(".autosd-demo.toml", "w") as fd:
            fd.write(output)
