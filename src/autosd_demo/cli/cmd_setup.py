import click

from autosd_demo.core.executor import get_executor
from autosd_demo.utils.build import get_build_env_config


@click.command
@click.option("-H", "--host", default=None)
def setup(host):
    """Setup demo environment"""
    is_remote = True if host else False
    build_env = get_build_env_config(host)

    executor = get_executor(remote=is_remote, become_required=True)
    with executor(host=host) as ctx:
        ctx.run_playbook("setup.yaml", extra_vars=build_env)
