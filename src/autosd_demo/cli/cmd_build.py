from datetime import datetime
from pathlib import Path

import click

from autosd_demo.core.executor import get_executor
from autosd_demo.utils.build import get_build_env_config


@click.command
@click.option("-H", "--host", default=None)
@click.option(
    "-d",
    "--distro",
    help="Distro used as image base to build the demo",
    default="autosd",
    show_default=True,
)
@click.option(
    "-t",
    "--target",
    help="Target used to build the demo              ",
    default="qemu",
    show_default=True,
)
@click.option(
    "-t",
    "--image-type",
    help="Image type used to build the demo          ",
    default="regular",
    show_default=True,
)
@click.option(
    "-a",
    "--image-arch",
    help="Image architecture                         ",
    default="x86_64",
    show_default=True,
)
@click.option(
    "-e",
    "--image-extension",
    help="Image extension                            ",
    default="qcow2",
    show_default=True,
)
@click.option(
    "--build-id",
    help="ID to be used for the build",
    default=f"{datetime.now():%Y%m%d_%H%M%S}",
)
@click.option(
    "--clean/--no-clean",
    is_flag=True,
    help="Clean the build folder after the build",
    default=True,
)
def build(
    host, distro, target, image_type, image_arch, image_extension, build_id, clean
):
    """Build demo"""
    is_remote = True if host else False
    build_env = get_build_env_config(host)

    build_env["build"].update(
        {
            "build_id": build_id,
            "distro": distro,
            "target": target,
            "image_type": image_type,
            "image_arch": image_arch,
            "image_name": (
                f"{distro}-{target}-{build_env['conf']['name']}"
                f"-{image_type}.{image_arch}.{image_extension}"
            ),
        }
    )

    executor = get_executor(remote=is_remote, become_required=True)
    with executor(host=host) as ctx:
        ctx.run_playbook("sudo-nopassword-enable.yaml", extra_vars=build_env)
        ctx.run_playbook("build-prepare-env.yaml", extra_vars=build_env)

        with ctx.cd(
            Path(
                build_env["build"]["build_dir"], build_env["build"]["build_id"], "src"
            ).as_posix()
        ):
            # FIXME: Improve the failure control here
            ctx.run(f"make {build_env['build']['image_name']}")
            ctx.run_playbook("build-download-image.yaml", extra_vars=build_env)

        if clean:
            ctx.run_playbook("build-clean-env.yaml", extra_vars=build_env)
        ctx.run_playbook("sudo-nopassword-disable.yaml", extra_vars=build_env)
