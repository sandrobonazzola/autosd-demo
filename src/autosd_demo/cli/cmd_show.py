import tempfile
from pathlib import Path

import click
from dynaconf import loaders

from autosd_demo.core.executor import get_executor
from autosd_demo.settings import settings
from autosd_demo.utils.build import get_build_env_config


@click.group()
def show():
    """Show information about the demo"""
    pass


@show.command()
@click.option(
    "-o",
    "--output-format",
    default="toml",
    help="Output format",
    type=click.Choice(["toml", "json", "yaml"]),
)
def config(output_format):
    """Show demo's configuration"""
    with tempfile.TemporaryDirectory() as tmpdir:
        exported_file = tmpdir + "/export"
        data = {key.lower(): val for key, val in settings.as_dict().items()}

        if output_format == "json":
            loaders.json_loader.write(exported_file, data)
        elif output_format == "yaml":
            loaders.yaml_loader.write(exported_file, data)
        else:
            loaders.toml_loader.write(exported_file, data)

        with open(exported_file) as fd:
            print(fd.read())


@show.command(name="osbuild-manifest")
def osbuild_manifest():
    """Show the osbuild manifest rendered"""
    with tempfile.TemporaryDirectory() as tmpdir:
        build_env = get_build_env_config(None)
        build_env["build"].update({"tmp_dir": tmpdir})

        executor = get_executor(remote=False)
        with executor(host=None) as ctx:
            ctx.run_playbook("show-osbuild-manifest.yaml", extra_vars=build_env)

        with open(Path(tmpdir, "image.mpp.yml")) as fd:
            print(fd.read())
