import click

from autosd_demo import __version__


@click.command()
def version():
    """Get the autosd-demo version."""
    click.echo(click.style(f"{__version__}", bold=True))
