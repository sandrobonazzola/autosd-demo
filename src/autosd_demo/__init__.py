import os

__version__ = "0.0.0" + os.getenv("AUTOSD_DEMO_GIT_RELEASE_VERSION", default="")

import sys
from pathlib import Path

from .utils import in_virtualenv

AUTOSD_DEMO_DATA_PATH = Path(sys.prefix, "share", "autosd-demo")

if in_virtualenv():  # pragma: no cover
    dev_data_path = Path(sys.prefix).parent.joinpath("data", "share", "autosd-demo")
    if dev_data_path.exists():
        AUTOSD_DEMO_DATA_PATH = dev_data_path

AUTOSD_DEMO_DEFAULT_CONF_PATH = Path(AUTOSD_DEMO_DATA_PATH, "conf")
