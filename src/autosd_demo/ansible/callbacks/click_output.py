import rich
from ansible.plugins.callback import CallbackBase

DOCUMENTATION = """
    author: Roberto Majadas
    name: click_output
    type: stdout
    short_description: Show stdout output in autosd-demos
    description:
        - Show stdout output in autosd-demos
    extends_documentation_fragment:
      - default_callback
    requirements:
      - set as stdout in configuration
"""


class CallbackModule(CallbackBase):  # type: ignore[misc]
    CALLBACK_VERSION = 2.0
    CALLBACK_TYPE = "stdout"
    CALLBACK_NAME = "autosd.demo.click_output"

    CALLBACK_NEEDS_ENABLED = False

    def v2_runner_on_failed(self, result, ignore_errors=False):
        rich.print(f"[red][■] {result._task.get_name()}[/red]")

    def v2_runner_on_ok(self, result):
        if result._result.get("changed", False):
            rich.print(
                "[green][[/green][yellow]🗸[/yellow][green]][/green]",
                f"[green]{result._task.get_name()}[/green]",
            )
        else:
            rich.print(f"[green][🗸] {result._task.get_name()}[/green]")

    def v2_runner_on_unreachable(self, result):  # pragma: no cover
        pass

    def v2_runner_on_skipped(self, result):  # pragma: no cover
        pass
