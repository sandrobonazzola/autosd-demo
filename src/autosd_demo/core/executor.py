import getpass
import sys
import tempfile
from pathlib import Path

import ansible
import click
import yaml
from ansible import constants as C
from ansible.cli.playbook import PlaybookCLI
from fabric import Connection
from invoke import AuthFailure, Config, Context

from autosd_demo.ansible.callbacks.click_output import (
    CallbackModule as ClickOutputCallback,
)
from autosd_demo.settings import settings
from autosd_demo.utils.ansible import get_ansible_playbook_path_from_name


class AnsibleExecutor:
    def run_playbook(self, playbook, extra_vars={}):
        C.DEFAULT_STDOUT_CALLBACK = ClickOutputCallback()

        with tempfile.TemporaryDirectory() as tmpdir:
            ansible_cmd = ["ansible-playbook"]
            if isinstance(self, Connection):
                ansible_cmd += [
                    "-i",
                    f"{self.host},",
                    "-u",
                    self.user,
                    "-e",
                    f"ansible_ssh_port={self.port}",
                ]
            else:
                ansible_cmd += ["-i", "localhost,", "-e", "ansible_connection=local"]

            if extra_vars != {}:
                extra_vars_path = Path(tmpdir, "extra-vars.yaml")
                ansible_cmd += ["-e", f"@{extra_vars_path.as_posix()}"]

                with open(extra_vars_path, "w") as fd:
                    yaml.dump(extra_vars, fd, default_flow_style=False)

            playbook_path = get_ansible_playbook_path_from_name(playbook)
            ansible_cmd += [playbook_path.as_posix()]

            if hasattr(
                ansible.utils.vars.load_extra_vars, "extra_vars"
            ):  # pragma: no cover
                # Remove ansible extra_vars cache between executions
                del ansible.utils.vars.load_extra_vars.extra_vars

            cli = PlaybookCLI(ansible_cmd)
            cli.ask_passwords = lambda: (None, self.config["sudo"]["password"])
            cli.parse()
            cli.run()


def ensure_required_access(become_required=False, remote=None):
    credentials = {"sudo": None, "ssh": None}
    is_remote = True if remote else False

    if is_remote:
        ctx = Connection(
            host=remote["address"], port=remote["port"], user=remote["user"]
        )
    else:
        ctx = Context()

    if become_required:
        result = ctx.run("sudo -n true", hide="both", warn=True)
        if not result.ok:
            sudo_pass = click.prompt("Sudo password", hide_input=True)
            new_config = ctx.config
            new_config["sudo"]["password"] = sudo_pass
            ctx.config = new_config
            try:
                ctx.sudo("id", hide="both", warn=True)
            except AuthFailure:
                click.echo("sudo password is not valid. Abort!")
                sys.exit(1)

    if is_remote:
        ctx.close()

    credentials["sudo"] = ctx.config["sudo"]["password"]
    return credentials


def __init_executor__(self, host, config=None):
    if isinstance(self, Connection):
        host_conf = {}
        if "remote_host" in settings and host in settings.remote_host.keys():
            host_conf = settings.remote_host[host]

        if "address" not in host_conf:
            host_conf["address"] = host
        if "port" not in host_conf:
            host_conf["port"] = 22
        if "user" not in host_conf:
            host_conf["user"] = getpass.getuser()

        credentials = ensure_required_access(
            become_required=self.become_required, remote=host_conf
        )
        new_config = Config()
        new_config["sudo"]["password"] = credentials["sudo"]
        Connection.__init__(
            self,
            host=host_conf["address"],
            config=new_config,
            port=host_conf["port"],
            user=host_conf["user"],
        )
    else:
        credentials = ensure_required_access(become_required=self.become_required)
        new_config = Config()
        new_config["sudo"]["password"] = credentials["sudo"]
        Context.__init__(self, config=new_config)

    AnsibleExecutor.__init__(self)


def __enter_executor__(self):
    if isinstance(self, Connection):
        return Connection.__enter__(self)
    return self


def __exit_executor__(self, *exc):
    if isinstance(self, Connection):
        Connection.__exit__(self, *exc)


def get_executor(remote=None, become_required=False):
    if remote:
        return type(
            "Executor",
            (Connection, AnsibleExecutor),
            {
                "__init__": __init_executor__,
                "__enter__": __enter_executor__,
                "__exit__": __exit_executor__,
                "become_required": become_required,
            },
        )
    else:
        return type(
            "Executor",
            (Context, AnsibleExecutor),
            {
                "__init__": __init_executor__,
                "__enter__": __enter_executor__,
                "__exit__": __exit_executor__,
                "become_required": become_required,
            },
        )
