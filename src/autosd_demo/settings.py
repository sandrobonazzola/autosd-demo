from dynaconf import Dynaconf

from autosd_demo import AUTOSD_DEMO_DATA_PATH, AUTOSD_DEMO_DEFAULT_CONF_PATH
from autosd_demo.utils import find_project_settings


def enrich_container_settings(settings):
    if "containers" not in settings:
        return

    for container_name, container_data in settings["containers"].items():
        container_defaults = [("version", "latest")]
        for key, value in container_defaults:
            if key not in container_data:
                settings["containers"][container_name][key] = value

    for container_name, container_data in settings["containers"].items():
        container_systemd_defaults = [
            ("unit", "description", f"{container_name} container"),
            ("container", "container_name", container_name),
            (
                "container",
                "image",
                f"localhost/{container_name}:" + container_data["version"],
            ),
            ("install", "wanted_by", "multi-user.target"),
            ("service", "restart", "always"),
        ]

        for section, key, value in container_systemd_defaults:
            if (
                ("systemd" not in container_data)
                or (
                    "systemd" in container_data
                    and section not in container_data["systemd"]
                )
                or (
                    "systemd" in container_data
                    and section in container_data["systemd"]
                    and key not in container_data["systemd"][section]
                )
            ):
                if "systemd" not in settings["containers"][container_name]:
                    settings["containers"][container_name]["systemd"] = {}
                if section not in settings["containers"][container_name]["systemd"]:
                    settings["containers"][container_name]["systemd"][section] = {}
                settings["containers"][container_name]["systemd"][section][key] = value


class Settings(Dynaconf):  # type: ignore[misc]
    def __init__(self):
        if find_project_settings() is not None:
            super().__init__(
                root_path=find_project_settings().parent.as_posix(),
                includes=[AUTOSD_DEMO_DEFAULT_CONF_PATH / "includes" / "*.toml"],
                preload=[AUTOSD_DEMO_DEFAULT_CONF_PATH / "preload" / "*.toml"],
                merge_enabled=True,
                environments=True,
                env_switcher="AUTOSD_DEMO_ENV",
                envvar_prefix="AUTOSD_DEMO",
                settings_files=[".autosd-demo.toml"],
                centos_sig={
                    "autosd_demo": {"share_dir": AUTOSD_DEMO_DATA_PATH.as_posix()}
                },
            )
            enrich_container_settings(self)
            self.unset("GIT_RELEASE_VERSION")
        else:
            super().__init__()


settings = Settings()


def reload_settings():
    global settings
    settings = Settings()
